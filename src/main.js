import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

Vue.config.productionTip = false

import Index from '@/page/index'
import Fav from '@/page/fav'

const router = new VueRouter({
  routes: [
    { path: '/', component: Index },
    { path: '/fav', component: Fav }
  ]
})

router.afterEach((to) => {
  // eslint-disable-next-line no-undef
  gtag('config', 'UA-135020208-1', {
    page_path: to.fullPath
  })
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
